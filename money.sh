#!/bin/bash
in=Money.csv
dest=./
quiz=money.adoc
theme=black
source ./params.sh
#[[ -f ~/Documents/Jula/brainbrew/Jula/src/data/Money.csv ]] && cp -v ~/Documents/Jula/brainbrew/Jula/src/data/Money.csv .

for x in gawk shuf csvtool; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

[[ -d ./node_modules/reveal.js/dist/theme/images ]] || mkdir ./node_modules/reveal.js/dist/theme/images/
cp  "./theme/$theme.css" node_modules/reveal.js/dist/theme/
cp  ./theme/*.css node_modules/reveal.js/dist/theme/
cp -r "./theme/fonts/noto-sans" node_modules/reveal.js/dist/theme/fonts/
cp ./images/manden* node_modules/reveal.js/dist/theme/images/
#cp -R node_modules -t $dest
#cp node_modules/reveal.js/js/reveal.js public/_/js/

cat <<EOF > $quiz
= Wariko karan lon o lon !
:revealjs_theme: $theme
:source-highlighter: highlight.js
:title-slide-transition: zoom
:title-slide-background-image: cfa.orig.jpg
:title-slide-background-size: 100% 
:title-slide-background-opacity: .4
:revealjs_transition: concave 
//:revealjs_slidenumber: true
:revealjs_controlsTutorial: true
:revealjs_navigationMode: linear
//:revealjs_shuffle: false
//:revealjs_embedded: true
//:revealjs_plugins: node_modules/reveal-ga/dist/reveal-ga.min.js
//:revealjs_plugins_configuration: reveal-ga-conf.js
:customcss: custom.css
:imagesdir: ./images
:docinfo: shared
:icons: font

[.r-fit-text]
Venez ici chaque jour pour devenir habile au marché avec les 'Dɔrɔme!' 
La flêche droite affiche un montant en F CFA et ensuite en Jula.

====
**I ni lɔgɔ !**
====

image::cfa.orig.jpg[background, size=cover]

EOF

csvtool -t COMMA -u TAB cat $in | grep argent | \
shuf -n 5 | awk -v quiz=$quiz -F "\t" '
{ hint=srand() }
{ print "[background-color='#28cc2d']" >> quiz }
{ print "== ", $2, "\n\n" >> quiz}
{ if (!($6 == ""))
{ image = gensub(/(.*)(")(.*\.png)(.*)/, "\\3", "g", $6); 
 print "image::"image"[background, size=cover, opacity=.5]\n\n">> quiz }
}
{ print "[.r-fit-text,step=1]" >> quiz }
{ print "**"$5"**", "\n\n" >> quiz}'

csvtool -t COMMA -u TAB cat $in | grep argent | \
shuf -n 5 | awk -v quiz=$quiz -F "\t" '
{ hint=srand() }
{ print "[background-color='#3581D8']" >> quiz }
{ print "== ", $5, "\n" >> quiz}
{ print "[.r-fit-text,step=2]" >> quiz }
{ print "**"$2"**", "\n\n" >> quiz}
{ if (!($6 == ""))
{ image = gensub(/(.*)(")(.*\.png)(.*)/, "\\3", "g", $6);
 print "image::"image"[step=1]\n\n">> quiz }
}'

# printf "[.smokewhite]\n" >> $quiz
# printf "== Félicitations ! !\n\n" >> $quiz
# printf "Mais n'oubliez pas de revenir demain pour un nouveau jeu de 'Wari'.\n\n" >> $quiz
# 
# printf "image::cfa.orig.jpg[background, size=cover, opacity=.5]\n\n" >> $quiz
# 
printf "== https://karan.mandenkan.com[Mandenkan.com] vous invite ! !\n\n" >> $quiz

printf "[.r-fit-text]\n" >> $quiz
printf "Visiter notre site web: https://karan.mandenkan.com[Mandenkan.com] pour toutes les infos sur les cours de Bambara et Jula.\n\n" >> $quiz
printf "Entre temps apprenez les https://karan.mandenkan.com/mandenkan/slides/colors/[couleurs en Jula]\n\n" >> $quiz
printf "Ou bien étudiez les https://karan.mandenkan.com/mandenkan/slides/5words/[cinq phrases quotidiennes] (qui change chaque jour) +\n" >> $quiz

printf "An bɛ kɔfɛ !\n\n" >> $quiz

npx asciidoctor-revealjs -D $dest  *.adoc 

